<?php

namespace App\Http\Controllers;

use App\KeyValue;
use Carbon\Carbon;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;

class KvStorage extends Controller
{
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $payload = $request->all();

        $this->validateRequest([
            'payload' => $payload,
        ]);

        foreach ($payload as $key => $value) {
            try {
                DB::beginTransaction();
                KeyValue::resetCurrent($key);
                $model = new KeyValue([
                    'created_at' => Carbon::now(),
                    'key' => $key,
                    'value' => $value,
                    'is_current' => 1
                ]);
                if ($model->save()) {
                    DB::commit();
                    return response()->json(['success'=>true]);
                }
                throw new Exception('Server error');
            } catch (Exception $e) {
                DB::rollback();
                return response()->json(['message'=>$e->getMessage()], 500);
            }
        }

        return response()->json(['success'=>false]);
    }

    /**
     * Display the specified resource.
     *
     * @param  string  $key
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, $key)
    {
        $prepare = KeyValue::where('key', $key);
        if ($request->timestamp) {
            $time = Carbon::createFromTimestamp($request->timestamp);
            $prepare->where('created_at', $time->toDateTimeString());
        } else {
            $prepare->current();
        }

        $value = $prepare->first();

        return response($value ? $value->value : '');
    }

    /**
     * Validates data
     * @throws ValidationException
     */
    protected function validateRequest($data)
    {
        Validator::make($data,
            [
                'payload' => 'required|size:1'
            ],
            [
                'payload.size' => 'The key value pair allowed is only 1.',
            ]
        )->validate();
    }

}
