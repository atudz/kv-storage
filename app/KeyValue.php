<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class KeyValue extends Model
{
    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = false;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'created_at',
        'key',
        'value',
        'is_current'
    ];

    /**
     * Local scope for current
     */
    public function scopeCurrent($query)
    {
        return $query->where('is_current',1);
    }

    /**
     * Local scope for current
     */
    public function scopeResetCurrent($query, $key)
    {
        return $query->where('key', $key)->update(['is_current'=>0]);
    }
}
