<?php

require __DIR__.'/../vendor/autoload.php';

$dotenv = Dotenv\Dotenv::createImmutable(__DIR__.'/../');
$dotenv->load();

$dsn = 'mysql:host='.getenv('DB_HOST');
$user = getenv('DB_USERNAME');
$password = getenv('DB_PASSWORD');
$database = getenv('DB_DATABASE');

try {
    $dbh = new PDO($dsn, $user, $password);
    $dbh->exec("CREATE DATABASE IF NOT EXISTS kv_test");
    echo 'Database '.$database ." created\n";
} catch (PDOException $e) {
    echo 'Connection failed: ' . $e->getMessage()."\n";
}