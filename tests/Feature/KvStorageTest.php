<?php

namespace Tests\Feature;

use Carbon\Carbon;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;
use Illuminate\Support\Str;

class KvStorageTest extends TestCase
{
    use RefreshDatabase;

    /**
     * Test Create
     *
     * @return void
     */
    public function testStore()
    {
        $key = Str::random(5);
        $value = Str::random(5);
        $response = $this->json('POST', '/object', [$key => $value]);

        $response
            ->assertStatus(200)
            ->assertJson([
                'success' => true,
            ]);
    }

    /**
     * Test Update
     *
     * @return void
     */
    public function testUpdate()
    {
        $key = Str::random(5);
        $value1 = Str::random(5);
        $time1 = Carbon::now()->timestamp;
        $response = $this->json('POST', '/object', [$key => $value1]);
        $response
            ->assertStatus(200)
            ->assertJson([
                'success' => true,
            ]);

        $value2 = Str::random(5);
        $response = $this->json('POST', '/object', [$key => $value2]);
        $response
            ->assertStatus(200)
            ->assertJson([
                'success' => true,
            ]);

        $response = $this->get('/object/'.$key);
        $response
            ->assertStatus(200)
            ->assertSee($value2);


        $response = $this->get('/object/'.$key.'?timestamp='.$time1);
        $response
            ->assertStatus(200)
            ->assertSee($value1);
    }

    /**
     * Test Create
     *
     * @return void
     */
    public function testFailure()
    {
        $response = $this->json('POST', '/object', []);
        $response->assertStatus(422);

        $key = Str::random(5);
        $key2 = Str::random(5);
        $value = Str::random(5);
        $response = $this->json('POST', '/object', [
            $key => $value,
            $key2 => $value
        ]);
        $response->assertStatus(422);

    }
}
